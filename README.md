
Ridge and Lasso Regression (L2 and L1 regularization) Using Python


What is regularization? 
Regularization is an advanced technique to reduce the errors due to overfitting when you try to fit the function into the training datasets. The different regularization techniques are L1, L2, and dropout regularization.
Here the main focus will be on L1 and L2 regression. Lasso and Ridge regression, respectively. 
 
Ridge (L2) and Lasso Regression (L1) are cogent machine learning techniques that make things more acceptable. Regularization helps to prevent the overfitting of data so that developers can learn from various datasets. To make sure the coefficients lean more towards the zero. The prime purpose here is to penalize the complex models so that their output reflects more losses. 
The complex models indicate two things: 
Complex enough to lead to overfitting. 
Or else complex computational challenges as in the latest system. It is most common when coming across millions of data. 
 
Although it seems that both work towards one goal, the inherent properties. And industrial use cases will always vary later. The key difference between the two is how they assign penalties to the coefficients:
 
Ridge Regression:
Performs L2 regularization, adding penalty equal to the square of the magnitude of coefficients
Minimization objective = LS Obj + * (sum of squared coefficients).
 
Lasso Regression:
Performs L1 regularization, which means adding penalty equal to the absolute magnitude of coefficients.
Minimization objective = LS Obj + * (sum of absolute values of coefficients).
 
Note that the acronym LS Obj refers to the least-squares regression objective without regularization.
Why do we use regression analytics?
Regression analysis is a predictive modeling technique that finds the relationship between two variables: dependent variables and independent variables. This technique has the best uses for forecasting, time series analysis, and others. This technique is an add-on benefit for data analysts and data scientists to process the best state of values for building robust predictive models.  
And the benefits are:

It highlights the significant relationship between the dependent and independent variables.
It highlights the strength of the impact of multiple independent variables on a dependent variable. 
Regularization techniques 
Ridge Regression Techniques 
Ridge regression is an advanced technique of linear equations. The prime purpose of this equation is to balance the RSS by adding the penalty. And the penalty equals the square of the magnitude of the coefficient. Even though the least square remains unbiased, they result in large variances. The output deviates from the real value. Therefore, if you add the one-sided value to the regression estimates, you can reduce the ridge regression errors. 
 
y=a+ b*x+e [ e= error terms ]

 
Here the green part is the original equation, and the red part is the penalty that got included in the equation to balance the RHS. 
Points To Keep in Mind 
The assumptions of this regression are the same as the least-squares regression. The only exceptional case is normality is not to get assumed.
Ridge regression reduces the value of coefficients but never reaches zero value. And it uses L2 Regularization. 
Lasso Regression Techniques 
LASSO stands for Least Absolute Shrinkage and Selection Operator. It is the most advanced technique used for finding out more accurate predictions. The model uses shrinkage techniques, where the value gets reduced till it leans towards a central point as mean. Using the Lasso model, we can solve simple and sparse models best suited for multicollinearity or during automation processes, such as variable/parameter selection. 
 
Lasso regression techniques use L1 regularization techniques. You can use this technique when you have more number of features as it automatically performs feature selection. 
The mathematical equation of Lasso Regression:
Residual sum of squares + λ * (Addition of the absolute value of the magnitude of coefficients).
 
 

Limitation Of Lasso Regression 
If the number of predictor variables (p) is greater than the number of observers (n), Lasso will choose n predictors as non-zero. And when all predictors are relevant, it starts to struggle in those cases. 
 
Lasso randomly selects from two or more collinear variables, which may or may not be useful for data interpretation. 
How do L1 and L2 regularization different from each other?
Regularization techniques prioritize prediction over interference. Ridge and lasso regression allows you to shrink the coefficients towards zero by adding a penalty in either case. The main advantages of these methods are that they permit you to go for complex models while avoiding overfitting at the same time. 
 
You set meta-parameters to define the amount of aggression for both lasso and ridge regularization. For ridge, alpha or L2 is the meta parameter, while for the lasso regression. Lambda or L1 is the meta parameter.
Implementation of L1 and L2 in Python 
Since we have got various ideas and concepts on Ridge and Lasso regression technique or L1 and L2 regularization in other words. It’s time to experience these concepts practically using the python programming language. 
 
First thing first, let’s import the frequent libraries that will make our work simpler and raise our performance through data manipulation and plotting. Then the next step is importing the dataset, looking into its rows and columns. 
 
The problem we’ll be solving here is a given set of features that describe a house in Boston, and our machine learning algorithms must be able to predict the house price. And here, we will be importing the Boston dataset and have a clear picture of its data frame contents.  
 
 

 
The very next step is to visualize the rows and columns. This process is known as Exploratory Data Analysis or EPA. The Boston dataset has 506 rows and 14 columns. These columns are crim, zn, indus, chas, nox, rm, age, dis, rad, tax, ptratio, black, istat, and medv. 
 

 
Now, using linear regression, ridge, and lasso regression, our first step is to find out the squared errors and compare all three. Then find out which one gives the best results. 
 
Firstly, we are importing the linear regression and cross_value_score objects. The first object will allow fitting into the linear model. The second object will perform k-fold cross-validation. Then we define our features and target variables for the entire model. The cross_val_score will return an array of MSE for each cross_validation step. 
 
So here, in this case, we have five of them. Thus, we can take the mean of MSE and print it. The value we’re getting is a negative MSE, and its value -37.1318.  
 

The next step here to check out which regression method works the best for the data models. Ridge or Lasso Regression. For ridge regression, let’s introduce GridSearchCV. It is an advanced algorithm that will automatically allow performing 5-fold-cross-validation with a range of different regularization parameters to find out the optimum value of alpha. 
 
Now you could see the optimal value of alpha is 100, with a negative MSE value of -29.90570. And we can easily distinguish the slight improvement in comparison with the basic multiple linear regression. The code for it looks like this. 
 
 

Lasso Regression 
We follow the same process for lasso regression that we followed for ridge regression. And in this case, the optimal value for the alpha is 1, and the MSE value we get is negative, that is -35.5315, which is the best score of all the three models. 
 

Conclusion
In this article, we talked about regularization. The L1 and L2 regularization. Also known as ridge regression (L2) and lasso regression (L1). Their formulae and how they are different from each other, along with their benefits too. We also talked about why we should use regression analysis and where to use it. Then we moved into the detailed explanation of ridge and lasso regression and then their live implementation using the python programming language.  


